package com.phoqus.paymentservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/health")
public class Controller {
    @GetMapping("/hello")
    public String health(){
        return "Hello World!";
    }
}
